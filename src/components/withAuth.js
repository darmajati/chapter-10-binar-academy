import React from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from '../services/firebase';
import { useRouter } from 'next/router';

const withAuth = (Component) => {
  const AuthComponent = (props) => {
    const [user, loading] = useAuthState(auth);
    const router = useRouter();

    if (loading) {
      return <div>Loading...</div>;
    }

    if (!user) {
      router.push('/login');
      return null;
    }

    return <Component {...props} />;
  };

  return AuthComponent;
};

export default withAuth;
