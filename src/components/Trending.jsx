import React from 'react';
import { Card, Button, CardImg, CardTitle, CardText, CardGroup,
 CardSubtitle, CardBody } from 'reactstrap';
import Link from "next/link"

const Trending = (props) => {
  return (
    <CardGroup>
      <Card>
        <CardImg top width="100%" style={{ width: "100%", height: "180px" }} src='./images/rockpaperstrategy-1600.jpg' alt="Card image cap" />
        <CardBody>
          <Button className='btn-warning'><Link style={{textDecoration: 'none', color: "black"}} href='/game'>Play</Link></Button>
        </CardBody>
      </Card>
      <Card>
        <CardImg top width="100%" style={{ width: "100%", height: "180px" }} src='./images/block2024io.jpeg' alt="Card image cap" />
        <CardBody>
          <Button className='btn-warning'>Play</Button>
        </CardBody>
      </Card>
      <Card>
        <CardImg top width="100%" style={{ width: "100%", height: "180px" }} src='./images/helix-jump-cover.avif' alt="Card image cap" />
        <CardBody>
          <Button className='btn-warning'>Play</Button>
        </CardBody>
      </Card>
      <Card>
        <CardImg top width="100%" style={{ width: "100%", height: "180px" }} src='./images/among-us.png' alt="Card image cap" />
        <CardBody>
          <Button className='btn-warning'>Play</Button>
        </CardBody>
      </Card>
      <Card>
        <CardImg top width="100%" style={{ width: "100%", height: "180px" }} src='./images/bloxd.avif' alt="Card image cap" />
        <CardBody>
          <Button className='btn-warning'>Play</Button>
        </CardBody>
      </Card>
    </CardGroup>
  );
};

export default Trending;