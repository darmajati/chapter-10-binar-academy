import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { auth } from '../services/firebase';
import { setEmail } from '../redux/reducers/login/loginSlice';
import { onAuthStateChanged,signOut } from 'firebase/auth';

export default function Navbar({children}) {
  const email = useSelector((state) => state.login.email);
  const dispatch = useDispatch();

  React.useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      if (user) {
        dispatch(setEmail(user.email));
      }
    });
    return unsubscribe;
  }, [dispatch]);

  const handleSignOut = () => {
    signOut(auth).then(() => {
      localStorage.removeItem('token');
      dispatch(setEmail(""));
    });
  };

    const UserMenu = () => {
    if (email) {
      return (
        <>
          <ul className="navbar-nav d-flex">
            <li className="nav-item">
              <a className="nav-link" href="/home">
                {email}
              </a>
            </li>
            <li className="nav-item">
              <button className="btn btn-link nav-link" onClick={handleSignOut}>
                LOGOUT
              </button>
            </li>
          </ul>
        </>
      );
    } else {
      return (
        <>
          <ul className="navbar-nav d-flex">
            <li className="nav-item">
              <a className="nav-link active" aria-current="page" href="/register">
                SIGN UP
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/login">
                LOGIN
              </a>
            </li>
          </ul>
        </>
      );
    }
  };

  const NavbarStyle = {
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  };

  return (
    <nav style={NavbarStyle} className="navbar navbar-expand-lg navbar-dark">
      <div className="container">
        <a className="navbar-brand me-5" href="#">
          LOGO
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <a className="nav-link active ms-5 fw-bold" aria-current="page" href="/">
                HOME
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#about">
                ABOUT
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#features">
                GAME FEATURES
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#requirements">
                SYSTEM REQUIREMENT
              </a>
            </li>

            <li className="nav-item">
              <a className="nav-link" href="#top-scores">QUOTES</a>
            </li>
          </ul>
          <UserMenu/>
        </div>
      </div>
    </nav>
  );
    {children}
}

