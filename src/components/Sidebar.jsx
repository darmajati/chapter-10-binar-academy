import React, { useState } from "react";
import Link from "next/link"
import {useSelector, useDispatch} from 'react-redux'
import { FaStar, FaSignOutAlt } from "react-icons/fa";
import { Container, Row, Col } from "reactstrap";
import Head from "next/head"
import { setEmail } from '../redux/reducers/login/loginSlice';
import { onAuthStateChanged,signOut } from 'firebase/auth';
import { auth, db } from '../services/firebase';
import { collection, getDocs, query, where } from "firebase/firestore";
import { getDownloadURL, getStorage, ref } from "firebase/storage";

export default function Sidebar({ children }) {
  const email = useSelector((state) => state.login.email);
  const dispatch = useDispatch();
  const [user, setUser] = useState({
    image : '',
    fullname : ''
  })

  const getDataFromFirestore = async (email) =>{
    let data = {
      imageURL : '',
      fullname : ''
    }
    const querySnapshot = await getDocs(query(collection(db, "users"), where("email", "==", email)));
    querySnapshot.forEach((doc) => {
      // doc.data() is never undefined for query doc snapshots
      data = {
        imageURL : doc.data().photoURL,
        fullname : doc.data().fullname
      }
    });
    
    if(data.imageURL){
      const storage = getStorage();
      const starsRef = ref(storage, data.imageURL);
      /* GET PHOTO PROFILE */
      getDownloadURL(starsRef)
        .then((url) => {
            // Insert url into an <img> tag to "download"
            setUser({ 
              ...user, image : url, 
              fullname : data.fullname 
            })
        }).catch((error) => console.log(error))
    }
    
  }

  React.useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      if (user) {
        dispatch(setEmail(user.email));
        getDataFromFirestore(user.email)
      }
    });
    return unsubscribe;
  }, [dispatch]);

  const handleSignOut = () => {
    signOut(auth).then(() => {
      localStorage.removeItem('token');
      dispatch(setEmail(""));
    });
  };

  return (
    <>
    <Head>
        <link rel="stylesheet" href="/css/sidebar.css"/>
    </Head>
      <Container fluid>
        <Row>
          <Col xs="2">
            <nav className="sidebar">
              <ul className="sidebar__list">
                <li className="text-center">
                  <img src={user.image} className="rounded-circle profile-picture mb-3" />
                </li>
                <li className="text-center profile">{user.fullname}</li>
                <li className="text-center profile mb-5">{email}</li>
                <li>
                  <Link href="/" className="sidebar__link">
                    <FaStar className="star-icon" /> Home
                  </Link>
                </li>
                <li>
                  <Link href="/gamelist" className="sidebar__link">
                    <FaStar className="star-icon" /> List Game
                  </Link>
                </li>
                <li>
                  <Link href="/users" className="sidebar__link">
                    <FaStar className="star-icon" /> List Profile
                  </Link>
                </li>
                <li>
                  <Link href="/leaderboard" className="sidebar__link">
                    <FaStar className="star-icon" /> Leaderboard
                  </Link>
                </li>
                <li>
                  <Link href="/" className="logout" onClick={handleSignOut}>
                    <FaSignOutAlt className="star-icon" /> Logout
                  </Link>
                </li>
              </ul>
            </nav>
          </Col>
          <Col xs="10">
            {children}
          </Col>
        </Row>
      </Container>
    </>
  );
}
