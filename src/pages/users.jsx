import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { Container, Row, Col, Card, Button, CardTitle, CardText, CardSubtitle, CardBody, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input } from "reactstrap";
import { collection, doc, getDocs, updateDoc } from "firebase/firestore";
import Styles from '../styles/listuser.module.css';
import { db, auth } from '../services/firebase';
import Sidebar from "@/components/Sidebar";

export default function ListUser() {
  const [users, setUsers] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null); // added state to keep track of selected user
  const userCollectionRef = collection(db, "users");
  const currentUser = auth.currentUser;
  const router = useRouter();

  useEffect(() => {
    const getUsers = async () => {
      const data = await getDocs(userCollectionRef);
      setUsers(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    }

    getUsers()
  }, [])

  const chunkedUsers = users.reduce((resultArray, item, index) => { 
    const chunkIndex = Math.floor(index / 3);

    if (!resultArray[chunkIndex]) {
      resultArray[chunkIndex] = []
    }

    resultArray[chunkIndex].push(item);

    return resultArray;
  }, []);

  const updateUser = async (id, fullname, email) => {
    try {
      const userDocRef = doc(db, 'users', id);
      await updateDoc(userDocRef, { fullname, email });
      alert('User information updated successfully');
    } catch (error) {
      console.error('Error updating user information:', error);
      alert('Error updating user information');
    }
  }

  const toggleModal = (user) => {
    setSelectedUser(user);
  }

  const handleUpdateUser = () => {
    updateUser(selectedUser.id, selectedUser.fullname, selectedUser.email);
    toggleModal(null);
  }

  const currentUserEmail = currentUser ? currentUser.email : null;
  
  return (
    <div className={Styles.background}>
      <Container fluid>
        <Row>
          <Col xs="3">
          <Sidebar/>
          </Col>
  
          <Col className="p-3">
            {chunkedUsers.map((row, i) => {
              return (
                <Row key={i}>
                  {row.map((user) => {
                    const isCurrentUser = user.email === currentUserEmail;
                    console.log(user.email);
  
                    return (
                      <Col md="4" key={user.id} className="mb-5">
                        <Card style={{ width: '20rem', backgroundColor: 'black' }}>
                          <img alt="Sample" src={'https://picsum.photos/300/200'} height="150" />
                          <CardBody style={{ padding: '25px' }} className={Styles.card}>
                            <CardTitle tag="h5">{user.fullname}</CardTitle>
                            <CardSubtitle className="mb-2 text-muted" tag="h6">
                              {user.email}
                            </CardSubtitle>
                            <CardText>Rank: Mythic</CardText>
                            {isCurrentUser ? (
                              <Button className={Styles.button} onClick={() => toggleModal(user)}>
                                Update Profile
                              </Button>
                            ) : (
                              <Button className={Styles.button} onClick={() => toggleModal(user)}>
                                See Profile
                              </Button>
                            )}
                          </CardBody>
                        </Card>
                      </Col>
                    );
                  })}
                </Row>
              );
            })}
  
            {/* modal component */}
            <Modal isOpen={selectedUser !== null} toggle={() => toggleModal(null)}>
              <ModalHeader className="text-black" toggle={() => toggleModal(null)}>
                Profile Details
              </ModalHeader>
              <img alt="Sample" src="https://picsum.photos/300/200" />
              <ModalBody style={{ padding: '25px' }}>
                <Form>
                  <FormGroup>
                    <Label className="text-black" for="fullname">
                      Fullname
                    </Label>
                    <Input
                      id="fullname"
                      name="fullname"
                      placeholder="fullname"
                      type="text"
                      defaultValue={selectedUser && selectedUser.fullname}
                      disabled={currentUserEmail !== selectedUser?.email}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label className="text-black" for="email">
                      Email
                    </Label>
                    <Input
                      id="email"
                      name="email"
                      placeholder="Email"
                      type="email"
                      defaultValue={selectedUser && selectedUser.email}
                      disabled={currentUserEmail !== selectedUser?.email}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label className="text-black" for="rank">
                      Rank
                    </Label>
                    <Input
                      id="rank"
                      name="rank"
                      placeholder="Rank"
                      type="text"
                      defaultValue={'Mythic'}
                      disabled="true"
                    />
                  </FormGroup>
                </Form>
                {currentUserEmail === selectedUser?.email && (
                  <Button onClick={handleUpdateUser} className={Styles.button}>
                    Edit Profile
                  </Button>
                )}
              </ModalBody>
              <ModalFooter style={{ padding: '5px 25px' }}>
                <Button className={Styles.button} onClick={() => toggleModal(null)}>
                  Close
                </Button>
              </ModalFooter>
            </Modal>
          </Col>
        </Row>
      </Container>
    </div>
  );
  
}
