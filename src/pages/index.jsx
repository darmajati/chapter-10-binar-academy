import Navbar from "../components/navbar";
import Head from "next/head";
import "bootstrap/dist/css/bootstrap.min.css";
import Carousel from "../components/Carousel";
import Footer from "../components/footer"
import Link from "next/link"

export default function LandingPage({ children }) {
  return (
    <>
      <Head>
        <title>The Games</title>
        <link rel="stylesheet" href="/css/LandingPage.css" />
        <link rel="icon" type="image/png" href="/game-images/logo 1.png"/>
      </Head>
      <section id="main-screen">
        <Navbar/>
          <div>
            <div className="text-center text-white highlight-text">
              <h1 className="text-uppercase fw-bold mb-4">Play Traditional Game</h1>
              <p className="mb-4">Experience new traditional game play</p>
              <Link href="/home">
              <button type="button" className="btn btn-warning text-uppercase fw-bold button">
                Play Now
              </button>
              </Link>
              
            </div>
            <div className="text-uppercase text-white" id="the-story">
              <p>The Story</p>
              <img src="./images/scroll-down.svg" alt="scroll-down" />
            </div>
          </div>
      </section>

      <section id="about">
        <div className="container">
          <div className="row">
            <div className="col-3 text-white text-uppercase">
              <p>What is so spesial?</p>
              <h2>The Games</h2>
            </div>
            <div className="col-9">
              <Carousel />
            </div>
          </div>
        </div>
      </section>

      <section id="features">
        <div className="container text-white">
          <div className="row">
            <div className="col-md-4 offset-md-8">
              <h4>What is so special?</h4>
            </div>
            <div className="col-md-4 offset-md-8">
              <h2>Features</h2>
            </div>
            <br /> <br /> <br /> <br />
            <div className="col-md-4 offset-md-8">
              <h4>Traditional Games</h4>
            </div>
            <div className="col-md-4 offset-md-8 mb-3">
              <p>if you miss your child hood, we provide many traditional games here</p>{" "}
            </div>
            <div className="col-md-4 offset-md-8 mb-3">
              <h4>GAME SUIT</h4>
            </div>
            <div className="col-md-4 offset-md-8 mb-3">
              <h4>TBD</h4>
            </div>
          </div>
        </div>
      </section>

      <section id="requirements">
        <p className="text-center text-white">Can my computer run the game?</p>
        <div className="container">
          <div className="row text-white">
            <div className="col-md-6">
              <h2>System requirements</h2>
              <div className="col-md-12">
                <div className="container">
                  <div className="row row-cols-2 text-white">
                    <div className="col border 1px">
                      OS:
                      <p>Windows 7 64 bit only (No OSX support at this time)</p>
                    </div>
                    <div className="col border 1px">
                      PROCESSOR:
                      <p>Intel Core 2 Duo @2.4 GHZ or AMD Athlon X2 @2.8 GHZ</p>
                    </div>
                    <div className="col border 1px">
                      Memory:
                      <p>4 GB RAM</p>
                    </div>
                    <div className="col border 1px">
                      Storage:
                      <p>8 GB available space</p>
                    </div>
                  </div>
                  <div className="row row-cols-1 col-md-13 text-white">
                    <div className="col border 1px">
                      GRAPHICS:
                      <p>
                        NVIDIA GeForce GTX 660 2GB or
                        <br />
                        AMD Radeon HD 7850 2GB DirectX11 (Shader Model 5)
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section id="top-scores">
        <div className="col">
          <div className="row">
            <div className="col-lg-4 offset-lg-1 top-score-text">
              <h2 className="mb-4">TOP SCORE </h2>
              <p className="mb-4">This top score from various games provided on this platform</p>
              <button type="button" className="btn btn-warning fw-bold">
                see more
              </button>
            </div>
            <div className="col">
              <div className="row">
                <div className="col-lg-9 offset-lg-4 mb-4">
                  <img src="./images/quote-1.png" alt="quote1" />
                </div>
                <div className="col-lg-9 offset-lg-2 mb-4">
                  <img src="./images/quote-2.png" alt="quote2" />
                </div>
                <div className="col-lg-9 offset-lg-4 mb-4">
                  <img src="./images/quote-3.png" alt="quote3" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section id="newsletter">
        <div className="container col">
          <div className="row">
            <div className="col-lg-4 offset-lg-1 newsleteter-image">
              <img src="./images/skull.png" alt="skull" />
            </div>
            <div className="col" id="newsletter-text">
              <div className="row text-white">
                <div className="col-lg-9 offset-lg-3 mb-4">
                  <p>Want to stay in touch?</p>
                  <h2 className="text-uppercase">NEWSLETTER SUBSCRIBE</h2>
                  <p className="mb-5">
                    In order to start receiving our news, all you have to do is enter your email address. Everything else will be taken care of by us. We will send you emails containing information about game. We don’t spam.
                  </p>
                  <form>
                    <input type="email" placeholder="Your Email Address" />
                    <button type="submit" className="btn btn-warning main-btn button">
                      Subscribe now
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
}
