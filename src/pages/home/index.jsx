import React from "react";
import NewsBanner from "../../components/NewsBanner";
import Trending from "../../components/Trending";
import Head from "next/head";
import 'bootstrap/dist/css/bootstrap.min.css';
import Sidebar from "../../components/Sidebar"
import withAuth from "@/components/withAuth";
import '@sweetalert2/theme-dark';

function Home() {
  return (
    <Sidebar>
      <Head>
        <title>The Games</title>
        <link rel="stylesheet" href="/css/content.css" />
      </Head>
      <div>
        <h1 className="ms-3 mb-4">News Today</h1>
        <NewsBanner />
        <h1 className="ms-3 mb-4 mt-5">Trending Games</h1>
        <Trending />
      </div>
    </Sidebar>
  );
}

export default withAuth(Home)
